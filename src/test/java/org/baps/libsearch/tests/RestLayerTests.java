package org.baps.libsearch.tests;

import com.google.common.base.MoreObjects;
import org.baps.libsearch.Application;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.client.MockRestServiceServer;

import static org.springframework.test.web.client.match.MockRestRequestMatchers.*;
import static org.springframework.test.web.client.response.MockRestResponseCreators.*;

import org.springframework.web.client.RestTemplate;

import java.util.HashMap;

/**
 * Created by kunal_patel on 27/04/16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = Application.class)
public class RestLayerTests {

	@Autowired
	private RestTemplate restTemplate;

	// private RestTemplate restTemplate = new RestTemplate();
	private MockRestServiceServer server = MockRestServiceServer.createServer(restTemplate);

	@Test
	public void test() {
		server
				.expect(requestTo("/"))
				.andExpect(method(HttpMethod.GET))
				.andRespond(withSuccess("{ \"id\" : 42, \"name\" : \"Holiday Inn\"}", MediaType.APPLICATION_JSON));

		Resp r = restTemplate.getForObject("/", Resp.class);
		server.verify();
		System.out.println(r);
	}


}

class Resp {
	int id;
	String name;

	public Resp() {
	}

	public Resp(int id, String name) {
		this.id = id;
		this.name = name;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("id", id)
				.add("name", name)
				.toString();
	}
}

package org.baps.libsearch.tests;

import org.springframework.stereotype.Service;

/**
 * Created by kunal_patel on 27/04/16.
 */
@Service
public class RemoteQueryService {

	public RemoteQueryService() {
		System.out.println("In default constructor of " + RemoteQueryService.class.getName());
	}
}

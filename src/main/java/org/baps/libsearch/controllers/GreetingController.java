package org.baps.libsearch.controllers;

import org.baps.libsearch.model.Article;
import org.baps.libsearch.services.InMemoryIndexingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PreDestroy;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Created by kunal_patel on 13/04/16.
 */
@RestController
public class GreetingController {

	private static final Logger logger = LoggerFactory.getLogger(GreetingController.class);

	@Autowired
	private InMemoryIndexingService indexingService;

	public GreetingController() {
		logger.info("Registering GreetingController !!");
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	@ResponseStatus(code = HttpStatus.OK)
	public Map<String, String> greet(HttpServletRequest request) {
		String details = String.format("receieved request from %s with %s", request.getRemoteAddr(), request.getHeader("user-agent"));
		logger.info(details);
		HashMap<String, String> hashMap = new HashMap<>(2);
		hashMap.put("message", "hello");
		hashMap.put("details", details);
		return hashMap;
	}


	@RequestMapping(value = "/index", method = RequestMethod.PUT)
	@ResponseStatus(code = HttpStatus.CREATED)
	public void index(@RequestBody Article aArticle) {
		logger.info("Indexing request recieved for article = {}", aArticle);
		indexingService.indexArticle(aArticle);
	}


	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public ResponseEntity<Article> search(@RequestParam String id) {
		logger.info("Searching article  with id = {}", id);
		Optional<Article> articleOptional = indexingService.searchArticle(id);
		if (articleOptional.isPresent())
			return new ResponseEntity<>(articleOptional.get(), HttpStatus.OK);
		else {
			logger.warn("Article not found for id = {}", id);
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}


	@PreDestroy
	private void cleanup() {
		logger.info("Called Cleanup method !!");
	}

}

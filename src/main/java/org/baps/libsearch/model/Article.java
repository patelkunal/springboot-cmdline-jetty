package org.baps.libsearch.model;

import com.google.common.base.MoreObjects;

/**
 * Created by kunal_patel on 13/04/16.
 */
public class Article {

	private String title;
	private String id;
	private String text;

	public Article() {
	}

	public Article(String title, String id, String text) {
		this.title = title;
		this.id = id;
		this.text = text;
	}

	public String getTitle() {
		return title;
	}

	public Article setTitle(String title) {
		this.title = title;
		return this;
	}

	public String getId() {
		return id;
	}

	public Article setId(String id) {
		this.id = id;
		return this;
	}

	public String getText() {
		return text;
	}

	public Article setText(String text) {
		this.text = text;
		return this;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
					.add("title", title)
					.add("id", id)
					.add("text", text)
					.toString();
	}
}

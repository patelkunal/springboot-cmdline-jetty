package org.baps.libsearch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.Banner;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.client.RestTemplate;

/**
 * Created by kunal_patel on 13/04/16.
 */
@SpringBootApplication
public class Application {

	private static final Logger logger = LoggerFactory.getLogger(Application.class);

	public static void main(String[] args) {
		logger.info("Starting StringBoot Application !!");
		ConfigurableApplicationContext context = new SpringApplicationBuilder()
					.bannerMode(Banner.Mode.OFF)
					.sources(Application.class)
					.run(args);
        Application application = context.getBean(Application.class);
        logger.info("Started StringBoot Application !!");
	}

	@Bean
	protected RestTemplate createRestTemplate() {
		return new RestTemplate();
	}

}

package org.baps.libsearch.services;

import com.google.common.collect.Maps;
import org.baps.libsearch.model.Article;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.Map;
import java.util.Optional;

/**
 * Created by kunal_patel on 13/04/16.
 */
@Service
public class InMemoryIndexingService {

	private static final Logger logger = LoggerFactory.getLogger(InMemoryIndexingService.class);

	private static final Map<String, Article> artcilesMap = Maps.newConcurrentMap();

	public InMemoryIndexingService() {
		logger.info("Registering InMemoryIndexingService");
	}

	@PostConstruct
	public void init() {
		logger.info("Populating IndexingService with some default articles in memory !!");
		this.indexArticle(new Article().setId("article1").setText("article1_text").setTitle("article1_title"));
		this.indexArticle(new Article().setId("article2").setText("article2_text").setTitle("article2_title"));
		logger.info("Populated IndexingService with some default articles in memory !!");
	}

	@PreDestroy
	public void clean() {
		artcilesMap.clear();
	}

	public void indexArticle(final Article article) {
		logger.info("Indexing article with id = {}", article.getId());
		artcilesMap.put(article.getId(), article);
		logger.info("Indexed article with id = {}", article.getId());
	}

	public Optional<Article> searchArticle(final String id) {
		logger.info("Searching for article with id = {}", id);
		return artcilesMap.containsKey(id) ? Optional.of(artcilesMap.get(id)) : Optional.empty();
	}
}
